﻿<%@ WebHandler Language="C#" Class="submitForm" %>

using System;
using System.Web;
using System.Net;
using System.Net.Mail;
using System.Web.Configuration;

public class submitForm : IHttpHandler {

	
	public void ProcessRequest (HttpContext context) {
		//context.Response.ContentType = "text/plain";
		//context.Response.Write("Hello World");

		string sname = context.Request.Params["sname"];
		string sphone = context.Request.Params["sphone"];
		string semail = context.Request.Params["semail"];

		//context.Response.Write(sname + "; " + sphone + "; " + semail);

		bool status = SendEmail(sname, semail, sphone);

		if (status)
		{
			context.Response.Write("1");
		}
		else
		{
			context.Response.Write("0");
		}

		status = false;
	}

	private bool SendEmail(string name, string email, string phone)
	{
		bool status = false;

		var message = new MailMessage();
		message.To.Add(new MailAddress(WebConfigurationManager.AppSettings["RecipientMail"]));  // replace with receiver's email id  
		message.To.Add(new MailAddress(WebConfigurationManager.AppSettings["RecipientMailCC"]));
		message.From = new MailAddress(WebConfigurationManager.AppSettings["SenderMail"]);  // replace with sender's email id 
		message.Subject = "Message From " + email;
		message.Body = "Name: " + name + "<br>From: " + email + "<br>Phone: " + phone;
		message.IsBodyHtml = true;

		using (var smtp = new SmtpClient())
		{
			smtp.Credentials = CredentialCache.DefaultNetworkCredentials;
			smtp.Host = WebConfigurationManager.AppSettings["SenderSmtpHost"];
			smtp.EnableSsl = false;
			smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;

			smtp.Send(message);

			status = true;
		}

		return status;
	}

	public bool IsReusable
	{
		get
		{
			return false;
		}
	}

}