﻿$(document).ready(function () {

	//$('form').ajaxForm({ url: 'submitForm.ashx', type: 'post' });
	$('form').submit(function (e) {
		e.preventDefault();

		var form = $(this);
		$(form).find('input[type=submit]').attr('disabled', true);

		showPreloader();

		$.ajax({
			type: 'POST',
			url: 'submitForm.ashx',
			data: form.serialize(), // serializes the form elements.
			success: function (data) {
				//console.log(data);

				if ($.trim(data) === '1') {
					//console.log('Success');
					$('.preloader').fadeOut("100");							
					showPopUpBanner();
				}
				else {
					//console.log('Error');
					$('.preloader').fadeOut("100");
					showPopUpBanner();
					$('#popUp_content .text').html('לא הצלחנו לשלוח את ההודעה. אנא נסו שנית.').addClass('failed');
				}

				$(form).trigger('reset'); //clearing form after sending			
				$(form).find('input[type=submit]').removeAttr('disabled');
			},
			error: function (jqXHR, exception) {
				console.log(jqXHR.responseText);
			}
		});
	});


	//popup window
	function showPreloader() {
		$('.preloader').fadeIn("1000");
	}
	function showPopUpBanner() {
		$('.popUpBannerBox').fadeIn("2000");
	}
	$('.popUpBannerBox').click(function (e) {
		if (!$(e.target).is('.popUpBannerContent, .popUpBannerContent *')) {
			$('.popUpBannerBox').fadeOut("2000");
			return false;
		}
	});
	function closeButton1() {
		$('.popUpBannerBox').fadeOut("2000");
		return false;
	}
	// Close Button on click PopUp fadeOut after 2sec
	$('#closeButton').click(function () {
		$('.popUpBannerBox').fadeOut("2000");
		return false;
	});


});